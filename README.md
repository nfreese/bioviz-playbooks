# README #

Deploy and update BioViz.org content and applications using Ansible.

* * *

## Create a host

In the Loraine Lab, we're mainly using AWS resources to create hosts and deploy software.
So this repository includes some code for doing that.

To create and configure BioViz host in your AWS account:

* Create aws_vars.yml (see `example_aws_vars.yml`)
* Run `ansible-playbook aws.yml`

Loraine Lab developers you should *not* do this. Dr. Loraine will set you up with what you need.

* * *

## Set up or update the host

Once you have a host, do the following:

* Create an inventory file (see `example_inventory.ini`)
* Create setup_vars.yml (see `example_setup_vars.yml`)
* Run `ansible-playbook -i [ your inventory file ] aws.yml`

**Note:** You must provide a domain name in the inventory file.

* * *

## Test the site

If your site's domain name is not yet registered with a DNS server,
you can test it by "tricking" your browser and your host into thinking
that its domain name is whatever you want.

To do that:

* Edit your local /etc/hosts file to associate the domain name with the host IP address
* Modify the hosts /etc/hosts file in the same way 

* * *

## Note for Loraine Lab developers ##

Submit a pull request to add your public rsa key to `roles/pubkey/files/Develop/Add`

* * *

### Who do I talk to? ###

* Ann Loraine aloraine@uncc.edu
